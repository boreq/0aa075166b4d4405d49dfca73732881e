# Vehicle road

>You need a TaxiwayParking spot designated as ‘Vehicle’ and then some TaxiwayPaths connecting to the aircraft parking spots.
>I don’t think it matters what type of TaxiwayPaths you have (Vehicle or Taxi) because the truck will use Taxi paths if it cant find a Vehicle one.
>You don’t need to place a Fuel Truck, one will spawn at the ‘Vehicle’ parking.
>A Fuel parking spot has a ‘Fuel Box’ to refuel the aircraft parked in it so it is something different.

https://forums.flightsimulator.com/t/guide-how-to-create-a-new-airport-version-0-3/166902/192

# Previewing objects before placing them

>You can skip this by checking the “one-click placement” button at the bottom of the objects window. That’ll show it in the world without actually placing it, then you can click through the different hangars to see what they look like. When you find one you like just click in the world to place it, then uncheck “One-click placement” so you can click the building to move/rotate it.

https://forums.flightsimulator.com/t/visual-guide-to-all-the-hangar-buildings/298555/2

# Custom models: white textures 

>WildLynxPilot at FSDeveloper found a possible fix to white textures.
>https://www.fsdeveloper.com/forum/threads/blender2msfs-support-thread.448402/post-858416

>SOLUTION FOUND!
>My folder structure was as follows in all my sceneries

    MY_PACKAGE
        CONTENTINFO
            AIRPORT
                Thumbnail.jpg
        SCENERY
            airport.bgl
            airport_contents.bgl
            modellib.bgl
            TEXTURE
                all textures
        layout.json
        manifest.json

>In the layout,json this structure was correctly referenced and everything worked up until the 1.9.3 patch.
>Now, I restructured the files and folders in the SCENERY as follows:

    SCENERY
        GLOBAL
            SCENERY
                modellib.bgl
                TEXTURE
                    All textures
        WORLD
            SCENERY
                airport.bgl
                airport_contents.bgl

>Changed the references in layout.json to reflect this
>And now all textures are back!
>So after the patch the sim does not read the json and respect the definitions thera, but it requires that the folder structure >above is followed! Bad.

https://forums.flightsimulator.com/t/how-to-fix-missing-model-textures-after-patch-1-9-3-0/289256/2